package es.franam.microserviciopeliculasactorescliente.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.franam.microserviciopeliculasactorescliente.beans.Pelicula;
import es.franam.microserviciopeliculasactorescliente.paginador.PageRender;
import es.franam.microserviciopeliculasactorescliente.servicios.IPeliculasService;

@Controller
public class PeliculaController {

	@Autowired
	IPeliculasService peliculasService;

	@GetMapping(value = { "/", "/home", "" })
	public String home(Model model) {
		return "home";

	}

	@GetMapping("/nuevo")
	public String nuevo(Model model) {
		model.addAttribute("titulo", "Nuevo Pelicula");
		Pelicula pelicula = new Pelicula();
		model.addAttribute("pelicula", pelicula);
		return "peliculas/formPelicula";
	}

	@GetMapping("/buscar")
	public String buscar(Model model) {
		return "peliculas/searchPelicula";
	}

	@GetMapping("/listado")
	public String listadoPeliculas(Model model, @RequestParam(name = "page", defaultValue = "0") int page) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Pelicula> listado = peliculasService.buscarTodas(pageable);
		PageRender<Pelicula> pageRender = new PageRender<Pelicula>("/peliculas/listado", listado);
		model.addAttribute("titulo", "Listado de todos los Peliculas");
		model.addAttribute("listadoPeliculas", listado);
		model.addAttribute("page", pageRender);
		return "peliculas/listPelicula";
	}

	@GetMapping("/idPelicula/{id}")
	public String buscarPeliculaPorId(Model model, @PathVariable("id") Integer id) {
		Pelicula Pelicula = peliculasService.buscarPeliculaPorId(id);
		model.addAttribute("Pelicula", Pelicula);
		return "Peliculas/formPelicula";
	}

	@GetMapping("/nombre")
	public String buscarPorTitulo(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam("nombre") String titulo) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Pelicula> listado = peliculasService.buscarPorTitulo(titulo, pageable);
		PageRender<Pelicula> pageRender = new PageRender<Pelicula>("/listado", listado);
		model.addAttribute("titulo", "Listado de Peliculas por titulo");
		model.addAttribute("listadoPeliculas", listado);
		model.addAttribute("page", pageRender);
		return "peliculas/listPelicula";
	}

	@GetMapping("/genero")
	public String buscarPorGenero(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam("genero") String categoria) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Pelicula> listado = peliculasService.buscarPorGenero(categoria, pageable);
		PageRender<Pelicula> pageRender = new PageRender<Pelicula>("/listado", listado);
		model.addAttribute("titulo", "Listado de Peliculas por genero");
		model.addAttribute("listadoPeliculas", listado);
		model.addAttribute("page", pageRender);
		return "peliculas/listPelicula";
	}

	@GetMapping("/actor")
	public String buscarPorActor(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam("actor") String profesor) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Pelicula> listado = peliculasService.buscarPorActor(profesor, pageable);
		PageRender<Pelicula> pageRender = new PageRender<Pelicula>("/listado", listado);
		model.addAttribute("titulo", "Listado de Peliculas por profesor");
		model.addAttribute("listadoPeliculas", listado);
		model.addAttribute("page", pageRender);
		return "peliculas/listPelicula";
	}

	@PostMapping("/guardar/")
	public String guardarPelicula(Model model, Pelicula pelicula, RedirectAttributes attributes) {

		peliculasService.guardar(pelicula);
		model.addAttribute("titulo", "Nuevo Pelicula");
		attributes.addFlashAttribute("msg", "Los datos del Pelicula fueron guardados!");
		return "redirect:/peliculas/listado";
	}

	@GetMapping("/editar/{id}")
	public String editar(Model model, @PathVariable("id") Integer id) {
		Pelicula pelicula = peliculasService.buscarPeliculaPorId(id);
		model.addAttribute("titulo", "Editar Pelicula");
		model.addAttribute("pelicula", pelicula);
		return "peliculas/formPelicula";
	}

	@GetMapping("/borrar/{id}")
	public String eliminar(Model model, @PathVariable("id") Integer id, RedirectAttributes attributes) {
		peliculasService.eliminar(id);
		attributes.addFlashAttribute("msg", "Los datos del Pelicula fueron borrados!");
		return "redirect:/peliculas/listado";
	}

}
