package es.franam.microserviciopeliculasactorescliente.servicios;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import es.franam.microserviciopeliculasactorescliente.beans.Actor;

public interface IActoresService {

	Page<Actor> buscarTodos(Pageable pageable);

	Actor buscarActorPorId(Integer idActor);

	void guardar(Actor Actor);

	void eliminar(Integer idActor);
}
