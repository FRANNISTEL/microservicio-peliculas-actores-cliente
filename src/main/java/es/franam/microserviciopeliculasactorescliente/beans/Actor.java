package es.franam.microserviciopeliculasactorescliente.beans;

import java.util.Date;
import java.util.List;

public class Actor {

	private Integer idActor;

	private String nombre;

	private Date fecha;

	private String pais;

	private List<Pelicula> peliculas;

	public Actor(Integer idActor, String nombre, Date fecha, String pais, List<Pelicula> peliculas) {
		this.idActor = idActor;
		this.nombre = nombre;
		this.fecha = fecha;
		this.pais = pais;
		this.peliculas = peliculas;
	}

	public Actor() {
		super();
	}

	public Integer getIdActor() {
		return idActor;
	}

	public void setIdActor(Integer idActor) {
		this.idActor = idActor;
	}

	public List<Pelicula> getPeliculas() {
		return peliculas;
	}

	public void setPeliculas(List<Pelicula> peliculas) {
		this.peliculas = peliculas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

}
